��          �            x     y     �     �  (   �  $   �  *     $   :  !   _  -   �  $   �     �     �  @         A     U  r  p  )   �  %     $   3  7   X  6   �  J   �  V     W   i  V   �        (   9  5   b  x   �  *     &   <     	                                                                   
       Clean directory /boot Configuring the grub Creating Host-Only initramfs Creating Host-Only initramfs was skipped Failed to create Host-Only initramfs Failed to create initramfs safemode backup Failed to create kernel for safemode Fix /var/db/pkg and user profiles Fix files containt of net-misc/networkmanager Host-Only initramfs has been created Remove kernel modules Remove kernel sources The current gcc config will be automatically switch to the newly Updating icon cache installed gcc version (.*) Project-Id-Version: calculate-lib 3.2.0
Report-Msgid-Bugs-To: support@calculate.ru
PO-Revision-Date: 2013-10-09 12:01+0300
Last-Translator: Mir Calculate <support@calculate.ru>, Elena Gavrilova <e.vl.gavrilova@yandex.ru>
Language-Team: Russian <support@calculate.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Очистка директории /boot Конфигурирование grub Создание Host-Only initramfs Создание Host-Only initramfs пропущено Не удалось создать Host-Only initramfs Не удалось создать резервную копию initramfs Не удалось создать ядро для безопасного режима Исправление /var/db/pkg и пользовательских профилей Исправление файлов принадлежащих net-misc/networkmanager Host-Only initramfs создан Удаление модулей ядра Удаление исходного кода ядра Текущая конфигурация gcc будет автоматически переключена на новую Обновление кэша иконок установка gcc версии \1 