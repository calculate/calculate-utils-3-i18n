��    #      4  /   L           	                 5      V     w  :   �  "   �     �     
          =     B  '   N     v     �     �  '   �     �     �     �     
     "     1  
   5     @     M     c     y     �     �     �     �     �  s  �     d     g  A   o  9   �  C   �  ?   /  X   o  D   �  1   	  +   ?	  <   k	     �	     �	  C   �	  6   
  #   :
     ^
  '   ~
     �
     �
  $   �
  (   �
  !        A     T     l      �  &   �  !   �  (   �        <   @     }     �                                                          "                                                
                             	      !            #    :  Base DN Create new LDAP service password Failed to configure LDAP server! Failed to connect to LDAP server Failed to erase LDAP database Failed to generate password for {service} service: {error} Failed to remove server parameters Failed to restore LDAP Failed to start LDAP Generate new service password LDAP LDAP Server LDAP configuration manually interrupted LDAP pre-configure LDAP server configured! LDAP server removed! LDAP service is not configured properly Remove Remove service Removing LDAP service Restarting LDAP service Restoring LDAP Run Setup LDAP Setup Server Starting LDAP service Stopping LDAP service System configuration Unix LDAP branch not found Wrong base DN generate new service password remove service set base DN Project-Id-Version: calculate-ldap 3.5.0
Report-Msgid-Bugs-To: support@calculate.ru
PO-Revision-Date: 2016-11-14 16:18+0300
Last-Translator: Mir Calculate <support@calculate.ru>, Elena Gavrilova <e.vl.gavrilova@yandex.ru>
Language-Team: Russian <support@calculate.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 :  Base DN Создать новый пароль для сервиса LDAP Не удалось настроить сервер LDAP! Не удалось соединиться с сервером LDAP Не удалось очистить базу данных LDAP Не удалось создать пароль для сервиса {service}: {error} Не удалось удалить параметры сервера Не удалось восстановить LDAP Не удалось запустить LDAP Создать новый пароль для сервиса LDAP Сервер LDAP Настройка LDAP прервана пользователем Предварительная настройка LDAP Сервер LDAP настроен! Сервер LDAP удалён! Сервис LDAP не настроен Удалить Удалить сервис Удаление сервиса LDAP Перезапуск сервиса LDAP Восстановление LDAP Выполнить Настроить LDAP Настроить сервер Запуск сервиса LDAP Остановка сервера LDAP Настройка системы LDAP-ветка Unix не найдена Неправильный base DN создать новый пароль для сервиса удалить сервис установить base DN 