��    %      D  5   l      @     A  "   D  *   g     �     �  $   �  *   �  (   �  &        B     b     �     �     �     �     �     �          +     E  (   a     �     �  !   �  0   �          '     >     Y     e  8   �     �     �     �     �       Q       k	  D   n	  L   �	      
     
  H   )
  _   r
  i   �
  J   <  R   �  D   �  8     )   X  K   �  6   �  >        D  8   [  :   �  >   �  T     3   c  H   �  Y   �  a   :  >   �  '   �  /     8   3  /   l  V   �  5   �  8   )  9   b  M   �  )   �                         $                             "                         %          #                         
      !                                   	                        :  Configuration manually interrupted Creating the home directory for {ur_login} Desktop Execute Failed to configure the user account Failed to create an encrypted user profile Failed to create the fastlogin mark file Failed to determine the home directory Failed to encrypt the directory Failed to mount ecrypted data Failed to unmount directory %s Force configuration Logout manually interrupted Mounting encrypted data No X session user found Path %s exists Please specify the user name Recovering encrypted data Setting up the user profile The home directory contains mount points Unable to detect the X session Unable to log out Unable to send the logout command Unable to wait for completion of the user logout Unmouning user resources User %s does not exist User Account Configuration User Logout User account configuration User account {ur_login} has been successfully configured User logged out! User logout User password not found Waiting for the logout force configuration Project-Id-Version: calculate-desktop 3.1.8
Report-Msgid-Bugs-To: support@calculate.ru
PO-Revision-Date: 2015-12-06 00:29+0200
Last-Translator: Tsebro Mykhailo, Slobodyan Victor <mihalych127@gmail.com, sarumyan@i.ua>
Language-Team: Russian <support@calculate.ru>
Language: uk_UA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.3
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ../../calculate-desktop/pym
 :  Налаштування перервано користувачем Створення домашньої директорії для {ur_login} Робочий стіл Виконати Не вдалося налаштувати обліковий запис Не вдалося створити шифрований профіль користувача Не вдалося створити файл-мітку для швидкого входу в сеанс Не вдалося визначити домашню директорію Не вдалося використати шифровану директорію Не вдалося підключити шифровані дані Не вдалося відмонтувати шлях %s Налаштувати примусово Завершення сесії перервано користувачем Підключення шифрованих даних Не знайдено користувачів у X сесії Шлях %s існує Необхідно вказати користувача Перестворення шифрованих даних Налаштування профілю користувача Каталог користувача містить точки монтування Не вдалося визначити X сесію Не вдалося завершити сеанс користувача Не вдалося відправити команду завершення сеансу Не вдалося дочекатися завершення сеансу користувача Відключення ресурсів користувача Користувач %s не існує Налаштування користувача Завершення сеансу користувача Налаштування користувача Обліковий запис користувача {ur_login} налаштовано Сеанс користувача завершено! Завершення сеансу користувача Не знайдено пароль користувача Очікування завершення сеансу користувача налаштувати примусово 