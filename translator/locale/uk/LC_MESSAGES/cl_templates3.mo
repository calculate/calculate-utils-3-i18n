��          �            x     y     �     �  (   �  $   �  *     $   :  !   _  -   �  $   �     �     �  @         A     U  �  p  +   m  #   �  &   �  9   �  8     J   W  V   �  Q   �  R   K  $   �  *   �  7   �  n   &  (   �  ,   �     	                                                                   
       Clean directory /boot Configuring the grub Creating Host-Only initramfs Creating Host-Only initramfs was skipped Failed to create Host-Only initramfs Failed to create initramfs safemode backup Failed to create kernel for safemode Fix /var/db/pkg and user profiles Fix files containt of net-misc/networkmanager Host-Only initramfs has been created Remove kernel modules Remove kernel sources The current gcc config will be automatically switch to the newly Updating icon cache installed gcc version (.*) Project-Id-Version: calculate-lib 3.2.0
Report-Msgid-Bugs-To: support@calculate.ru
PO-Revision-Date: 2015-12-09 10:06+0200
Last-Translator: Tsebro Mykhailo, Slobodyan Victor <mihalych127@gmail.com, sarumyan@i.ua>
Language-Team: Russian <support@calculate.ru>
Language: uk_UA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.3
 Очищення директорії /boot Конфігуруванння grub Створення Host-Only initramfs Створення Host-Only initramfs пропущено Не вдалося створити Host-Only initramfs Не вдалося створити резервну копію initramfs Не вдалося створити ядро для безпечного режиму Виправлення /var/db/pkg та профілів користувачів Виправлення файлів, що належать net-misc/networkmanager Host-Only initramfs створено Видалення модулів ядра Видалення сирцевого коду ядра Поточна конфігурація gcc буде автоматично перемкнута на нову Оновлення кешу іконок встановлення gcc версії \1 