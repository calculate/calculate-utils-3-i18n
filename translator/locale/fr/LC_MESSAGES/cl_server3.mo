��          \      �       �   $   �   "   �            2     I  =   a     �  r  �  -   )  0   W  2   �  '   �  (   �  T     '   a                                       Failed to clear server configuration Failed to remove server parameters Failed to save server parameters LDAP server configured Samba server configured To manage Unix accounts, a configured LDAP server is required Unix server configured Project-Id-Version: calculate-server 3.5
Report-Msgid-Bugs-To: support@calculate.ru
PO-Revision-Date: 2016-12-12 12:01+0300
Last-Translator: Mir Calculate <support@calculate.ru>, Elena Gavrilova <e.vl.gavrilova@yandex.ru>
Language-Team: French <support@calculate.ru>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Impossible d'effacer les réglages du serveur Impossible de supprimer les réglages du serveur Impossible de sauvegarder les réglages du serveur Le serveur LDAP a bien été configuré Le serveur Samba a bien été configuré Un serveur LDAP configuré est nécessaire pour gérer les comptes utilisateurs Unix Le serveur Unix a bien été configuré 