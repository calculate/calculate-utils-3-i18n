��    %      D  5   l      @     A  "   D     g  *   y     �     �  $   �  *   �  &        +     K     i     �     �     �     �     �     �          .  (   J     s     �  !   �  0   �     �          '     B     N  8   i     �     �     �     �     �  u       x  +   |     �  -   �     �     �  .   �  3   ,	  -   `	  %   �	  *   �	  )   �	     	
  @   
     `
  #   ~
     �
  $   �
  &   �
  #     2   &  $   Y     ~  2   �  &   �  "   �       #   8     \  #   i  8   �  #   �     �  $   �          4                         $                             "                  	          %      #                         
              !                                                    :  Configuration manually interrupted Copying skel data Creating the home directory for {ur_login} Desktop Execute Failed to configure the user account Failed to create an encrypted user profile Failed to determine the home directory Failed to encrypt the directory Failed to mount ecrypted data Failed to unmount directory %s Force configuration Logout manually interrupted Mounting encrypted data No X session user found Path %s exists Please specify the user name Recovering encrypted data Setting up the user profile The home directory contains mount points Unable to detect the X session Unable to log out Unable to send the logout command Unable to wait for completion of the user logout Unmouning user resources User %s does not exist User Account Configuration User Logout User account configuration User account {ur_login} has been successfully configured User logged out! User logout User password not found Waiting for the logout force configuration Project-Id-Version: calculate-desktop 3.1.8
Report-Msgid-Bugs-To: support@calculate.ru
PO-Revision-Date: 2013-10-09 12:01+0300
Last-Translator: Mir Calculate <support@calculate.ru>, Elena Gavrilova <e.vl.gavrilova@yandex.ru>
Language-Team: French <support@calculate.ru>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
  :  Configuration interrompue par l'utilisateur Copie des données skel Création du répertoire home pour {ur_login} Bureau Valider Impossible de configurer le compte utilisateur Impossible de créer un profil utilisateur chiffré Impossible de déterminer le répertoire home Impossible de chiffrer le répertoire Impossible de monter les volumes chiffrés Impossible de démonter le répertoire %s Configuration forcée La fermeture de la session a été interrompue par l'utilisateur Montage des volumes chiffrés Aucun utilisateur pour la session X Le chemin %s existe Veuillez saisir le nom d'utilisateur Récupération des données chiffrées Configuration du profil utilisateur Le répertoire home contient des points de montage Impossible de détecter la session X Échec de fermeture de session La commande de déconnexion n'a pas été envoyée Temps d'attente de fermeture trop long Démontage des volumes utilisateur L'utilisateur %s n'existe pas Configuration du compte utilisateur Déconnexion Configuration du compte utilisateur Le compte utilisateur {ur_login} a bien été configuré L'utilisateur a fermé la session ! Déconnexion Mot de passe utilisateur introuvable En attente de fermeture configuration forcée 