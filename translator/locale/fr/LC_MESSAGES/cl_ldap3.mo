��    #      4  /   L           	                 5      V     w  :   �  "   �     �     
          =     B  '   N     v     �     �  '   �     �     �     �     
     "     1  
   5     @     M     c     y     �     �     �     �     �  p  �     a     e  .   m  (   �  *   �  -   �  H     0   g     �     �  ,   �     �     	  >   	     P	  '   j	  &   �	  1   �	  	   �	     �	     

     &
     C
  	   R
     \
     l
     �
     �
     �
     �
     �
  ,   �
     '     <                                                          "                                                
                             	      !            #    :  Base DN Create new LDAP service password Failed to configure LDAP server! Failed to connect to LDAP server Failed to erase LDAP database Failed to generate password for {service} service: {error} Failed to remove server parameters Failed to restore LDAP Failed to start LDAP Generate new service password LDAP LDAP Server LDAP configuration manually interrupted LDAP pre-configure LDAP server configured! LDAP server removed! LDAP service is not configured properly Remove Remove service Removing LDAP service Restarting LDAP service Restoring LDAP Run Setup LDAP Setup Server Starting LDAP service Stopping LDAP service System configuration Unix LDAP branch not found Wrong base DN generate new service password remove service set base DN Project-Id-Version: calculate-ldap 3.5
Report-Msgid-Bugs-To: support@calculate.ru
PO-Revision-Date: 2016-12-12 12:01+0300
Last-Translator: Mir Calculate <support@calculate.ru>, Elena Gavrilova <e.vl.gavrilova@yandex.ru>
Language-Team: French <support@calculate.ru>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
  :  Base DN Créer un nouveau mot de passe du serveur LDAP Impossible de configurer le serveur LDAP Impossible de se connecter au serveur LDAP Impossible d'effacer la base de données LDAP Impossible de créer un mot de passe pour le service {service} : {error} Impossible de supprimer les réglages du serveur Impossible de restaurer LDAP Impossible de démarrer LDAP Générer un nouveau mot de passe du service LDAP Serveur LDAP La configuration de LDAP a été interrompue par l'utilisateur Préconfiguration de LDAP Le serveur LDAP a bien été configuré Le serveur LDAP a bien été supprimé Le service LDAP n'est pas configuré correctement Supprimer Supprimer le service Suppression du serveur LDAP Redémarrage du service LDAP Restaurer LDAP Exécuter Configurer LDAP Configurer le serveur Démarrage du service LDAP Arrêt du serveur LDAP Configuration système Branch LDAP Unix introuvable Base DN incorrect générer un nouveau mot de passe du serveur supprimer le service définir le base DN 