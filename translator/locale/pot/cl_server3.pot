#: ../calculate-server/pym/server/server.py:116
msgid "Failed to clear server configuration"
msgstr ""

#: ../calculate-server/pym/server/server.py:124
msgid "Failed to remove server parameters"
msgstr ""

#: ../calculate-server/pym/server/server.py:101
msgid "Failed to save server parameters"
msgstr ""

#: ../calculate-server/pym/server/variables/server.py:54
msgid "LDAP server configured"
msgstr ""

#: ../calculate-server/pym/server/variables/server.py:77
msgid "Samba server configured"
msgstr ""

#: ../calculate-server/pym/server/variables/server.py:35
msgid "To manage Unix accounts, a configured LDAP server is required"
msgstr ""

#: ../calculate-server/pym/server/variables/server.py:66
msgid "Unix server configured"
msgstr ""
