#!/usr/bin/env python
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-install

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import glob
import sys

from setuptools import setup, Command
from setuptools.command.build import build as _build
from setuptools.command.build_py import build_py as _build_py
from setuptools.command.install_lib import install_lib as _install_lib
from setuptools.command.install_egg_info import install_egg_info

locales = ("ru", "bg", "fr", "uk", "lt")
msg_params = "--check-format --check-domain -o"

LANGUAGE_DIR = "translator/locale"

def cout(string):
    sys.stdout.write(string)
    sys.stdout.flush()


class build_po(Command):

    description = "compile message files to binary MO files"
    user_options = []

    def initialize_options(self):
        self.domain = 'messages'
        pass

    def finalize_options(self):
        pass

    def run(self):
        for locale in locales:
            localepath = os.path.join(LANGUAGE_DIR, locale, "LC_MESSAGES")
            po_path = f"{LANGUAGE_DIR}/{locale}"

            self.mkpath(po_path)
            self.mkpath(localepath)

            for po in glob.glob(f"{po_path}/*.po"):

                if "packages.po" in po or "new.po" in po:
                    continue

                addon = f"{po_path}/packages.po"
                if "cl_update" in po and os.path.exists(addon):
                    cmd = "msgfmt %s %s/%s %s %s" % (
                        msg_params,
                        localepath,
                        os.path.basename(po)[:-2] + "mo",
                        po,
                        addon,
                    )
                else:
                    cmd = "msgfmt %s %s/%s %s" % (
                        msg_params,
                        localepath,
                        os.path.basename(po)[:-2] + "mo",
                        po,
                    )
                cout(cmd + "\n")
                os.system(cmd)


class empty_egg_info(install_egg_info):
    def run(self):
        pass

class install_lib(_install_lib):
    def run(self):
        pass

class build(_build):

    def run(self):
        self.run_command('build_po')
        _build.run(self)

    def has_po(self):
        return any(len(glob.glob("%s/*.po" % x)) > 0 for x in ("ru", "fr"))

    sub_commands = _build.sub_commands + [("build_po", has_po)]

class build_py(_build_py):

    def run(self):
        self.run_command('build_po')
        _build_py.run(self)


s = setup(
    name="calculate-i18n",
    version="3.10.0",
    description="Calculate Linux internationalization",
    author="Calculate Ltd.",
    author_email="support@calculate.ru",
    url="http://calculate-linux.org",
    license="http://www.apache.org/licenses/LICENSE-2.0",
    data_files=[
        (
            os.path.join(locale, "LC_MESSAGES"),
            glob.glob(os.path.join(LANGUAGE_DIR, locale, "LC_MESSAGES/*.mo")),
        )
        for locale in locales
    ],
    cmdclass={
        "build": build,
        "build_py": build_py,
        "build_po": build_po,
    },
)

